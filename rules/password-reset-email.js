function (user, context, callback) {

    var count = context.stats && context.stats.loginsCount ? context.stats.loginsCount : 0;
    if (count <= 0) {

        //send reset password email 
        var AuthenticationClient = require('auth0').AuthenticationClient;

        var auth0 = new AuthenticationClient({
            domain: 'tanver-custom.au.auth0.com',
            clientId: 'DJomcD9YLGv1ZPB6baN1uoCTK5uVD9Jz'
        });
        var data = {
            email: user.email,
            connection: context.connection
        };
        auth0.changePassword(data, function (err, message) {
            if (err) {
                console.log(err);
            }
            console.log(message);
        });
        return callback(new UnauthorizedError('Please Reset your email.'));
    } else {
        return callback(null, user, context);
    }
}